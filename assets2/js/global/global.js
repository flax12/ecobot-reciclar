//redirigir al home
function redirectIndex(){
    location.reload();
}

function msjError(mensaje){
    Swal.fire({
        icon: 'error',
        title: 'Error!',
        text: mensaje
    })
}

function msjConfirmacin(titulo,mensaje){
    Swal.fire({
        title: titulo,
        text: mensaje,
        icon: 'success',
        confirmButtonText: 'OK'
    }).then((result) => {
        if (result.isConfirmed) {
            redirectIndex();
        }
    })
}