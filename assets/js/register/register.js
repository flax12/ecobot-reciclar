$(document).ready(function(){

  var base_url = $('#base_url').val();

    $("#formRegistro").validate({
        rules: {
          nombre_completo:{
            required: true
          },
          telefono:{
            required: true,
            maxlength: 10,
            minlength:10
          },
          email: {
            required: true,
            email: true
          },
          pass: {
            required: true
          },
          pass2: {
            required: true
          }
        },
        messages: {
          nombre_completo:{
            required: "Campo Obligatorio"
          },
          telefono:{
            required: "Campo Obligatorio",
            maxlength: 'Por favor tienes que ingresar maximo 10 numeros',
            minlength: 'Por favor tienes que ingresar minimo 10 numeros',
          },
          email: {
            required: "Campo Obligatorio",
            email: "Por favor ingrese un email valido"
          },
          pass:{
            required: "Campo Obligatorio"
          },
          pass2:{
            required: "Campo Obligatorio"
          }
        },
        submitHandler: function(form){
            
            //var formData = new FormData(document.getElementById('formRegistro'));
            
            $.ajax({

                url: base_url + "Registro/guardarRegistro",
                type: "POST",
                data: $("#formRegistro").serialize()
                
            }).done(function(data){
          
                var response = $.parseJSON(data);
                
                if(response.code == 1){
                    $.growl.notice({title: "Hola", message: response.mensaje, duration: 3000 });
                    $("#formRegistro")[0].reset();
                    setTimeout(login,3000);
                }
                if(response.code != 1){
                    $.growl.error({title: response.titulo, message: response.mensaje, duration: 3000 });
                    $("#formRegistro")[0].reset();
                }
                
            }).fail(function() {
                    alert( "error" );
                })
            .always(function() {
            });
        }
    });
}); 


//redirecionar al login
function login(){
  var base_url = $('#base_url').val();
  window.location = base_url;
} 

function validarPass(){
  console.log("entro");
  var pass1 = $("#pass").val();
  var pass2 = $("#pass2").val();

  if (pass1 != pass2) {
    $("#msgError").html("Las contraseñas no coinciden");
    $("#btnRegistrar").attr('disabled',true);
  }else{
    $("#msgError").html("");
    $("#btnRegistrar").attr('disabled',false);
  }
}   
