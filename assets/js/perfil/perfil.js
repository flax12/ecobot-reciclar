
var input=  document.getElementById('numeroDocumento');

input.addEventListener('input',function(){
    if (this.value.length > 12) 
        this.value = this.value.slice(0,12); 
})

function valideKey(evt) {
    var code = evt.which ? evt.which : evt.keyCode;

    if (code == 8) {
        //backspace
        return true;
    } else if (code >= 48 && code <= 57) {
        //is a number
        return true;
    } else {
        return false;
    }
}

$("#mostarPass").click(function(){

	var opcion = $("#mostarPass").val()

	if (opcion == 0) {
		document.getElementById('divPass').style.display="";
		$("#mostarPass").val(1);
	}else{
		document.getElementById('divPass').style.display="none";
		$("#mostarPass").val(0);
	}
});



function validarPassPerfil(){

  var pass1 = $("#pass").val();
  var pass2 = $("#pass2").val();

  if (pass1 != pass2) {
    $("#msgError").html("Las contraseñas no coinciden");
    $("#btnPerfil").attr('disabled',true);
  }else{
    $("#msgError").html("");
    $("#btnPerfil").attr('disabled',false);
  }
}  



$(document).ready(function(){

  var base_url = $('#base_url').val();

    $("#formPerfil").validate({
        rules: {
          numeroDocumento:{
            required: true
          },
          nombreCompleto:{
            required: true
          },
          tlfContacto:{
            required: true,
            maxlength: 10,
            minlength:10
          },
          cuidad:{
            required: true
          },
          email: {
            required: true,
            email: true
          },
          pass: {
            required: true
          },
          pass2: {
            required: true
          }
        },
        messages: {
          numeroDocumento:{
            required: "Campo Obligatorio"
          },
          nombreCompleto:{
            required: "Campo Obligatorio"
          },
          tlfContacto:{
            required: "Campo Obligatorio",
            maxlength: 'Por favor tienes que ingresar maximo 10 numeros',
            minlength: 'Por favor tienes que ingresar minimo 10 numeros',
          },
          cuidad:{
            required: "Campo Obligatorio"
          },
          email: {
            required: "Campo Obligatorio",
            email: "Por favor ingrese un email valido"
          },
          pass:{
            required: "Campo Obligatorio"
          },
          pass2:{
            required: "Campo Obligatorio"
          }
        },
        submitHandler: function(form){
            
            var formData = new FormData(document.getElementById('formPerfil'));
            
            $.ajax({

                url: base_url + "Usuario/updatePerfil",
                type: "POST",
                dataType: "html",
                data: formData,
                cache: false,
                contentType: false,
                processData: false
                
            }).done(function(data){
          
                var response = $.parseJSON(data);
                
                if(response.code == 1){
                    //$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
                    //$("#formPerfil")[0].reset();
                    //setTimeout(redireccionMismaPagina,2000);
                    Swal.fire({
                      title: '¡Listo!',
                      text: response.mensaje,
                      icon: 'success',
                      confirmButtonText: 'ok'
                    }).then((result) => {
                      if (result.isConfirmed) {
                        redireccionMismaPagina();
                      }
                    })
                }
                if(response.code != 1){
                    //$.growl.error({title: response.titulo, message: response.mensaje, duration: 2000 });
                    //$("#formPerfil")[0].reset();

                    Swal.fire({
                      icon: 'error',
                      title: 'ERROR!',
                      text: response.mensaje
                    })
                }
                
            }).fail(function() {
                  Swal.fire({
                    icon: 'error',
                    title: 'ERROR!',
                    text: "Error del sistema"
                  })
                })
            .always(function() {
            });
        }
    });
}); 

function redireccionMismaPagina(){
	location.reload();
}
