
//paput vista previa
function popUp(URL) {
    window.open(URL, 'Vista previa', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=800,height=400,left = 100,top = 50');
}


//envio formulario de general

var base_url = $("#base_url").val();

$("#btnGuardarGeneral").click(function(){

	var formData = new FormData(document.getElementById('formGeneral'));
	
	$.ajax({

        url: base_url + "Panel/processCambiosPlantilla",
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
                
    }).done(function(data){
          
        var response = $.parseJSON(data);
                
        if(response.code == 1){
            $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
            setTimeout(redirectEditTemplate,2000);
        }
        if(response.code != 1){
            $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
        }
                
        }).fail(function() {
            alert( "error" );
        })
        .always(function() {
    });
});


//guardar Servicios

$("#btnGuardarServicios").click(function(){

	$.ajax({
        type: "POST",
        url: base_url + "Panel/processCambiosPlantilla",
        data: $("#formServicios").serialize(),
        success: function(respuesta){
            var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
            	$.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            }
                
            if(response.code == 1){
               $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            	setTimeout(redirectEditTemplate,2000);
            }
                
        },
        dataType: "html"
    
    });
});    

//guardar Testimonios

$("#btnTestimonio").click(function(){

	var formData = new FormData(document.getElementById('formTestimonio'));
	
	$.ajax({

        url: base_url + "Panel/processCambiosPlantilla",
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
                
    }).done(function(data){
          
        var response = $.parseJSON(data);
                
        if(response.code == 1){
            $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
            setTimeout(redirectEditTemplate,2000);
        }
        if(response.code != 1){
            $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
        }
                
        }).fail(function() {
            alert( "error" );
        })
        .always(function() {
    });
});


//guardamos las fotos de galeria

$("#btnGaleria").click(function(){

    var formData = new FormData(document.getElementById('formGaleria'));
    
    $.ajax({

        url: base_url + "Panel/processCambiosPlantilla",
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
                
    }).done(function(data){
          
        var response = $.parseJSON(data);
                
        if(response.code == 1){
            $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
            setTimeout(redirectEditTemplate,2000);
        }
        if(response.code != 1){
            $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
        }
                
        }).fail(function() {
            alert( "error" );
        })
        .always(function() {
    });
});

//para la seccion de direccion 
$("#btnDireccion").click(function(){

    $.ajax({
        type: "POST",
        url: base_url + "Panel/processCambiosPlantilla",
        data: $("#formDireccion").serialize(),
        success: function(respuesta){
            var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
                $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            }
                
            if(response.code == 1){
               $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
                setTimeout(redirectEditTemplate,2000);
            }
                
        },
        dataType: "html"
    
    });
});

//SECCION ADMNISTRADIR

$("#btnAdministrador").click(function(){

    var formData = new FormData(document.getElementById('formAdminstrador'));
    
    $.ajax({

        url: base_url + "Panel/processCambiosPlantilla",
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
                
    }).done(function(data){
          
        var response = $.parseJSON(data);
                
        if(response.code == 1){
            $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
            setTimeout(redirectEditTemplate,2000);
        }
        if(response.code != 1){
            $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
        }
                
        }).fail(function() {
            alert( "error" );
        })
        .always(function() {
    });
});

/*seccion video*/
$("#btnVideo").click(function(){

    $.ajax({
        type: "POST",
        url: base_url + "Panel/processCambiosPlantilla",
        data: $("#formVideo").serialize(),
        success: function(respuesta){
            var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
                $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            }
                
            if(response.code == 1){
               $.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
                setTimeout(redirectEditTemplate,2000);
            }
                
        },
        dataType: "html"
    
    });
});

function redirectEditTemplate(){
	location.reload();
}