$(document).ready(function(){
   
    $("#btnLogin").click(function(){
        var base_url = $("#base_url").val();

        $.ajax({
            type: "POST",
            url: base_url + "Login/iniciarSesion",
            data: $("#formLogin").serialize(),
            success: function(respuesta){
                var response = $.parseJSON(respuesta);
                

                if (response.code != 1) {
                    //$.growl.error({title: "Hola", message: response.mensaje, duration: 3000 });
                    Swal.fire({
                        icon: 'error',
                        title: 'ERROR!',
                        text: response.mensaje
                    })
                }
                
                if(response.code == 1){
                    location.reload();
                }
                
            },
            dataType: "html"
        });
    });

    $(document).keypress(function(e) {
        if(e.which == 13) {
            var base_url = $("#base_url").val();

            $.ajax({
                type: "POST",
                url: base_url + "Login/iniciarSesion",
                data: $("#formLogin").serialize(),
                success: function(data){
                    var response = $.parseJSON(data);

                    if (response.code != 1) {
                        //$.growl.error({title: "Hola", message: response.mensaje, duration: 3000 });
                        Swal.fire({
							icon: 'error',
							title: 'ERROR!',
							text: response.mensaje
						})
                    }
                    if(response.code == 1){
                        location.reload();
                    }
                },
                dataType: "html"
            });
        }
    });
    
});

