
$("#btnRegistrarCupon").click(function(){
	
	var base_url = $("#base_url").val();

	$.ajax({
		
		url: base_url + "Panel/insertCupon",
		type: "POST",
		data: $("#formRegistrarCupon").serialize(),
		success: function(respuesta){
			
			var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
                //$.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
				Swal.fire({
					icon: 'error',
					title: 'ERROR!',
					text: response.mensaje
				})
			}
                
            if(response.code == 1){
                //$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
				Swal.fire({
					title: 'Hola',
					text: response.mensaje,
					icon: 'success',
					confirmButtonText: 'ok'
				}).then((result) => {
					if (result.isConfirmed) {
						$('#formRegistrarCupon')[0].reset();
					}
				})
            }
		} 
	});

});


function pasarIdCupon(idCupon){

	Swal.fire({
		title: 'Deseas Eliminar Este cupon?',
		text: "estos cambios seran permanentes",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si'
	  }).then((result) => {
		
		if (result.isConfirmed) {

			var base_url = $("#base_url").val();

			$.ajax({
				
				url: base_url + "Panel/deleteCupon",
				type: "POST",
				data: { idCupon: idCupon },
				success: function(respuesta){
					
					var response = $.parseJSON(respuesta);
						
					if (response.code != 1) {
						//$.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
						Swal.fire({
							icon: 'error',
							title: 'ERROR!',
							text: response.mensaje
						})
					}
						
					if(response.code == 1){
						//$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
						//setTimeout(redirectTablaCupones,2000);
						Swal.fire({
							title: 'Hola',
							text: response.mensaje,
							icon: 'success',
							confirmButtonText: 'ok'
						}).then((result) => {
							if (result.isConfirmed) {
								redirectTablaCupones();
							}
						})
					}
				} 
			});
		  
		}
	})
}

function pasarCuponModal(datos){
	var data = datos.split('||');

	$("#idCuponEditar").val(data[0]);
	$("#cupon").val(data[1]);

}

$("#btnEliminarCupon").click(function(){
	
	

});


$("#btnActualizarCupon").click(function(){
	
	var base_url = $("#base_url").val();

	$.ajax({
		
		url: base_url + "Panel/editarCupon",
		type: "POST",
		data: $("#formActualizarCupon").serialize(),
		success: function(respuesta){
			
			var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
                $.growl.error({title: "ERROR", message: response.mensaje, duration: 2000 });
            }
                
            if(response.code == 1){
				$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
				setTimeout(redirectTablaCupones,2000);
            }
		} 
	});

});


function redirectTablaCupones(){
	location.reload();
}

$("#btnRegistrarArchivo").click(function(){

	var base_url = $("#base_url").val();

    var formData = new FormData(document.getElementById('formRegistrarArchivo'));
    
    $.ajax({

        url: base_url + "Panel/insertCsvCupones",
        type: "POST",
        dataType: "html",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
                
    }).done(function(data){
          
        var response = $.parseJSON(data);
                
        if(response.code == 1){
            //$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
            //$("#formPerfil")[0].reset();
			//setTimeout(redirectEditTemplate,2000);
			
			Swal.fire(
				'Hola',
				response.mensaje,
				'success'
			)
			$("#formRegistrarArchivo")[0].reset();

        }
        if(response.code != 1){
			
			Swal.fire({
                icon: 'error',
                title: 'ERROR!',
                text: response.mensaje
            })
            $("#formRegistrarArchivo")[0].reset();
        }
                
        }).fail(function() {
            Swal.fire({
                icon: 'error',
                title: 'ERROR!',
            })
        })
        .always(function() {
    });
});


//elimiar cupones selecionados

function eliminarCuponesSelecionados() {
	var base_url = $("#base_url").val();

	var cupones = $('.form-check-input');

	var cuponesSeleccionados = new Array();

	for (let index = 0; index < cupones.length; index++) {
		
		//console.log(cupones[index].value);

		if(cupones[index].checked == true){
			//console.log(cupones[0].value);
			cuponesSeleccionados.push(cupones[index].value);
		}
		
	}

	var data = {
		cupones: cuponesSeleccionados
	}

	$.ajax({
		
		url: base_url + "Panel/DeleteCuponesSeleccionados",
		type: "POST",
		data: data,
		success: function(respuesta){
			var response = $.parseJSON(respuesta);
						
			if (response.code != 1) {
				Swal.fire({
					icon: 'error',
					title: 'ERROR!',
					text: response.mensaje
				})
			}
						
			if(response.code == 1){
				Swal.fire({
					title: 'Hola',
					text: response.mensaje,
					icon: 'success',
					confirmButtonText: 'ok'
				}).then((result) => {
					if (result.isConfirmed) {
						redirectTablaCupones();
					}
				})
			}
			
		} 
	});
}

