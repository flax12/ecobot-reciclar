
//funcion para pasar informacion al modal
function infoAModalUsuario(datos){
	data = datos.split('||');

    $("#tlf").html("Teléfono: " + data[0]);
    $("#cuidad").html("Ciudad: " + data[1]);
}

function botellas(datos){

    $("#textBotellas").html("Tienes un total de botellas de: " + datos);

}

function pasarIdUsuario(idUser){
	//$("#id_users").val(idUser);
	Swal.fire({
		title: 'Deseas Eliminar Este Usuario?',
		text: "estos cambios seran permanentes!",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si'
	  }).then((result) => {
		
		if (result.isConfirmed) {

			var base_url = $("#base_url").val();

			$.ajax({
				
				url: base_url + "Panel/deleteUser",
				type: "POST",
				data: { id_users : idUser },
				success: function(respuesta){
					
					var response = $.parseJSON(respuesta);
						
					if (response.code != 1) {
						//$.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
						Swal.fire({
							icon: 'error',
							title: 'ERROR!',
							text: response.mensaje
						})
					}
						
					if(response.code == 1){
						//$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
						//setTimeout(redirectTablaCupones,2000);
						Swal.fire({
							title: 'Hola',
							text: response.mensaje,
							icon: 'success',
							confirmButtonText: 'ok'
						}).then((result) => {
							if (result.isConfirmed) {
								redirectTablaUsers();
							}
						})
					}
				} 
			});
		  
		}
	})
}

/*$("#btnEliminarUser").click(function(){
	
	var base_url = $("#base_url").val();

	$.ajax({
		
		url: base_url + "Panel/deleteUser",
		type: "POST",
		data: $("#formEliminarUser").serialize(),
		success: function(respuesta){
			
			var response = $.parseJSON(respuesta);
                
            if (response.code != 1) {
                $.growl.error({title: "Hola", message: response.mensaje, duration: 2000 });
            }
                
            if(response.code == 1){
				$.growl.notice({title: "Hola", message: response.mensaje, duration: 2000 });
				setTimeout(redirectTablaUsers,2000);
            }
		} 
	});

});*/

function redirectTablaUsers(){
	location.reload();
}

