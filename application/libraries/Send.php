
<?php 

require 'PHPMailer/Exception.php';
require 'PHPMailer/PHPMailer.php';
require 'PHPMailer/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class send {

	//atributos globales
	private $email_vendedor = "fuentesricardo1995@gmail.com";
	private $pass_vendedor = 'javier26089057';
	//private $pass_vendedor = "";
	private $servido_smtp = 'smtp.gmail.com';


	public function enviarEmailVendedor($email){
			
		$mail = new PHPMailer(true);

		//CONFIGURACION
	 	$mail->isSMTP();                                          

	    $mail->Host = $this->servido_smtp;   

	    $mail->SMTPAuth   = true;                                   
	    $mail->Username = $this->email_vendedor;                     
	    $mail->Password   = $this->pass_vendedor;                               
	    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
	    $mail->Port = 587;

	    $mail->setFrom($this->email_vendedor, 'prueba');
	    $mail->addAddress($this->email_vendedor, 'Cliente');     // Add a recipient         //
	    
	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Compra pastillas negras';
	    $mail->Body    = 'Tiene un pedido por: ' . $email;
	    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	    $mail->send();
	}

	public function enviarEmailCliente($email, $link){
		
		$mail = new PHPMailer(true);

		try {

			//CONFIGURACION
		 	$mail->isSMTP();                                          
		    $mail->Host = $this->servido_smtp;   

		    $mail->SMTPAuth   = true;                                   
		    $mail->Username = $this->email_vendedor;                     
		    $mail->Password   = $this->pass_vendedor;                               
		    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
		    $mail->Port = 587;

			$mail->setFrom($this->email_vendedor, 'confimacion de Correo');
		    $mail->addAddress($email, 'Cliente');  

			$mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = 'confirmacion de Correo';
		    $mail->Body    = '
		    <!doctype html>
			<html lang="en">
			  
			  <body>
			    <h3>Por favor dar click en el siguiente Enlace para terminar el proceso de confirmacion</h3>
			    <div class="">
			    	<a class="btn btn-primary" href="'.$link.'" role="button">Confirmar</a>
			    </div>
			  </body>
			</html>
		    ';

		    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		    $mail->send();

		    return 1;

		} catch (Exception $e) {
		    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";

		    return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}    

	}

}

?>