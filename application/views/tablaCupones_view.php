<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listado de Cupones</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()."Panel" ?>">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado</h3><br>
              <a target="blank" class="btn btn-success" href="<?php echo base_url()."Panel/excelCupones" ?>" role="button">Exportar excel</a>
            </div>
            
            <!-- /.card-header -->
            <div class="table-responsive card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="width: 5px;">Selecionar</th>
                  <th>ID</th>
                  <th>Cupon</th>
                  <th>Estado</th>
                  <th>Borrar</th>
                </tr>
                </thead>
                <tbody>

                <form id="formDeleteCuponesSelecionados">

                <?php foreach($data as $value ) { 
                  
                  $datos = $value['id_cupones'].'||'.$value['cupon'];
                
                ?>
                  <tr>
                    <td>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="<?php echo $value['id_cupones'] ?>"  name="CheckEliminarCupones[]">
                      </div>
                    </td>

                      <td><?php echo $value['id_cupones'] ?></td>
                      <td><?php echo $value['cupon'] ?></td>
                      <td>
                      <?php 
                            if ($value['estatus'] == 1) {
                                echo  "ACTIVO";
                            }else{
                                echo  "INACTIVO";
                            }
                        ?>    
        
                      </td>

                      <td>
                            
                        <button  onclick="pasarIdCupon(<?php echo $value['id_cupones']; ?>)" type="button" class="btn btn-danger" >
                            Eliminar
                        </button>

                      </td>

                  </tr>
                <?php } ?>

                </form>
                  
                </tbody>

                <tfoot>
                  <tr>
                    <th>
                       <button onclick="eliminarCuponesSelecionados()" id="deletCuponesSelecionados" type="button" class="btn btn-danger" >
                            Eliminar Selecionados
                        </button>
                    </th>
                    
                  </tr>
                </tfoot>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Cupon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h4>Deseas eliminar este cupon?</h4>
        <form id="formEliminarCupon">
            <input type="hidden" name="idCupon" id="idCupon">
            <button type="button" id="btnEliminarCupon" class="btn btn-success">SI</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modal editar-->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">

            <div class="container">
               
                <form id="formActualizarCupon">
                    <input type="hidden" name="idCuponEditar" id="idCuponEditar"> 

                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="cupon">Cupon</label>
                          <input type="text" class="form-control" id="cupon" name="cupon" required> 
                        </div>
                      </div>

                      <div class="col-md-6" style="padding-top:31px;">
                        <button type="button" id="btnActualizarCupon" class="btn btn-primary btn-block">Acualizar</button>
                      </div>

                    </div>

                </form>
            
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

  </div>

<?php 
	$this->load->view('templates/footer'); 
?>