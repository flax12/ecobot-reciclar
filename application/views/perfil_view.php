<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Mi Perfil</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Panel">Home</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Datos</h3>
            </div>
            
            <!-- /.card-header -->
            <div class="container card-body">

              <form id="formPerfil">

                <h4 class="text-center my-4">Informaci&oacute;n personal</h4>

                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="tipoDocumento">Tipo Documento</label>
                        <select class="form-control" name="tipoDocumento" id="tipoDocumento">
                            <option value="">Selecciona tipo de documento</option>
                            <option value="1">Cédula de Ciudadanía</option>
                            <option value="2">Tarjeta de Identidad</option>
                            <option value="3">Pasaporte</option>
                            <option value="4">Cédula de Extranjería</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="numeroDocumento">N&uacute;mero de Documento</label>
                        <input type="number" class="form-control" name="numeroDocumento" id="numeroDocumento" onkeypress="return valideKey(event);" value="<?php echo $user[0]['numeroDocumento']?>">
                    </div>

                    
                    <div class="form-group">
                      <label for="">Nombre Completo</label>
                      <input type="text" class="form-control" id="nombreCompleto" name="nombreCompleto" required value="<?php echo $user[0]['nombreCompleto']?>">
                    </div>
                    
                    <div class="form-group">
                      <label for="tlfContacto">Telefono Contacto</label>
                      <input type="number" class="form-control" id="tlfContacto" name="tlfContacto" required value="<?php echo $user[0]['tlfContacto']?>">
                    </div>
                    
                    <div class="form-group">
                        <label for="cuidad">Ciudad</label>
                        <select class="form-control" name="cuidad" id="cuidad">
                            <option value="<?php echo $user[0]['cuidad']?>"><?php echo $user[0]['cuidad']?></option>
                            <option value="bogota">Bogotá</option>
                            <option value="medellin">Medellín</option>
                            <option value="cali">Cali</option>
                            <option value="barranquilla">Barranquilla</option>
                        </select>
                    </div>

                  </div>

                  <div class="col-md-6">

                    <div class="form-group">
                      <label for="email">Correo</label>
                      <input type="email" class="form-control" id="email" name="email" required value="<?php echo $user[0]['email']?>"> 
                    </div>

                    <div class="custom-control custom-switch text-center">
                      <input type="checkbox" class="custom-control-input" id="mostarPass" name="mostarPass" value="0">
                      <label class="custom-control-label" for="mostarPass">Deseas Cambiar La contraseña</label>
                    </div>

                    <div id="divPass" style="display: none">
                      <div class="form-group">
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="nueva contraseña"> 
                      </div>
                      <div class="form-group">
                        <input type="password" class="form-control" id="pass2" name="pass2" required placeholder="ingrese nuevamente la contraseña" onchange="validarPassPerfil()">
                        <span style="color: red;" id="msgError"></span>

                      </div>
                      
                    </div>

                  </div>

                </div>

                <div class="col-md-12 my-3">
                  <button type="submit" id="btnPerfil" class="btn btn-primary btn-block">Actualizar</button>
                </div>

              </form>  
              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>  

  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="#">Med<b>trip</b></a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>

<script>
  $("#tipoDocumento option[value="+ <?php echo $user[0]['tipoDocumento'] ?> +"]").attr("selected",true);
</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!--script de notificaciones-->
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/jquery.growl/js/jquery.growl.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/jquery-form-validation/jquery.validate.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url();?>assets/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<!--<script src="<?php echo base_url();?>assets/plugins/jqvmap/jquery.vmap.min.js"></script>-->
<!--<script src="<?php echo base_url();?>assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>-->
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url();?>assets/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url();?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url();?>assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url();?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url();?>assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="<?php echo base_url();?>assets/js/dashboard.js"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/js/demo.js"></script>

<!--script para pasar informacion al modal en la tabla de registros-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/informationTableUsuario/informationTableUsuario.js"></script>

<!--perfil-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/perfil/perfil.js"></script>


<!--plantillas editar-->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/processPlantilla/processPlantilla.js"></script>

<!-- script para procesar formulario de pago -->
<script type="text/javascript" src="<?php echo base_url()?>assets/js/cupon/cupon.js"></script>


<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>


</body>
</html>