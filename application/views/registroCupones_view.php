<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cupones</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Panel">Home</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Registro de Cupones</h3>
            </div>
            
            <!-- /.card-header -->
            <div class="container card-body">

              <form id="formRegistrarCupon">

                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="cupon">Cupon</label>
                      <input type="text" class="form-control" id="cupon" name="cupon" required> 
                    </div>
                  </div>

                  <div class="col-md-6" style="padding-top:31px;">
                    <button type="button" id="btnRegistrarCupon" class="btn btn-primary btn-block">Registrar</button>
                  </div>

                </div>
                
              </form>  

              <form id="formRegistrarArchivo">

                <div class="row">

                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="archivo">Subir csv de cupones</label>
                      <input type="file" class="form-control" id="archivo" name="archivo" required> 
                    </div>
                  </div>

                  <div class="col-md-6" style="padding-top:31px;">
                    <button type="button" id="btnRegistrarArchivo" class="btn btn-primary btn-block">Guardar</button>
                  </div>

                </div>
                
              </form>  
              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>  


<?php 
	$this->load->view('templates/footer'); 
?>