 <?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 

  $session = $this->session->all_userdata();

 ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <?php if($session['rol'] == 0){ ?>
              <h1 class="m-0 text-dark">Dashboard</h1>
            <?php } ?>  
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url();?>Panel">Home</a></li>
            </ol>
          </div><!-- /.col -->
          <?php if($session['rol'] == 1){ ?>
            <div class="col-md-12 text-center ">
              <h1 class="mb-3" style="font-size:70px;">¡Hola!</h1>
              <h4>
                Aquí podrás ver el histórico de cupones que has registrado. Te invitamos a seguir reciclando en Ecobot. Encuentra tu Ecobot más cercano en <a target="blank" href="https://www.ecobot.com.co/">www.ecobot.com.co</a>
              </h4>
            </div>
          <?php } ?> 
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

        <?php if($session['rol'] == 0) {  ?>
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                  <h3>1000</h3>

                  <p>Plastic Surgery</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>50</h3>

                  <p>Orthopedics</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>44</h3>

                  <p>User Registrations</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box">
                <div class="inner">
                  <h3>65</h3>

                  <p>Unique Visitors</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>

        <?php }else { ?>

          <div class="row">

              <?php 

                $contador = 0;

                $texto = "En";

                foreach ($botellas as $key => $value) { 
                  
                  if($contador){
                    $texto = "En ";
                  }
              ?>

                  <div class="col-12 col-lg-4">
                    <!-- small box -->
                    <div class="small-box">
                      <div class="inner">
                        <h2><?php echo $texto . " " . $key; ?></h2>
                        <h5>has acumulado <?php echo $value; ?> códigos registrados</h5>

                      </div>
                      <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                      </div>
                    </div>
                  </div>
                
              <?php 
                    $contador++;
                } 
              ?>

              <div class="col-12 col-lg-4 ">
                    <!-- small box -->
                    <div class="small-box">
                      <div class="inner">
                        <h2>Tu Total</h2>
                        <h5>de códigos registrados es:  <?php echo $total; ?></h5>

                      </div>
                      <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                      </div>
                    </div>
                  </div>

          </div>


        <?php } ?>
        <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>


  <?php 
    $this->load->view('templates/footer'); 
  ?>