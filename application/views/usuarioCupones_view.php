<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listado de Cupones</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()."Panel" ?>">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado</h3>
            </div>
            
            <!-- /.card-header -->
            <div class="table-responsive card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Cupon</th>
                  <th>fecha de Registro</th>
                  <th>Ver Botellas</th>
                </tr>
                </thead>
                <tbody>

                <?php 

                  $botellas = 0;

                  foreach($data as $value ) { 
                  
                  $botellas += 1;
                
                ?>
                  <tr>

                      <td><?php echo $value['id_cuponesUsuarios'] ?></td>
                      <td><?php echo $value['cupon'] ?></td>
                      <td>
                      <?php 
                           echo $value['fechaRegistro'];
                        ?>    
        
                      </td>

                      <td>
                        <button  onclick="botellas('<?php echo $botellas; ?>')" type="button" class="btn btn-success" data-toggle="modal" data-target="#modalbotella">
                            VER
                        </button>
                      </td>

                  </tr>
                <?php } ?>
                  
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>


  <!-- Modal -->
<div class="modal fade" id="modalbotella" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Cupon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h4 id="textBotellas"></h4>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<?php 
	$this->load->view('templates/footer'); 
?>