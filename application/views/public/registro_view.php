<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Stylish Portfolio - Start Bootstrap Template</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?php echo base_url() ?>assets2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="<?php echo base_url() ?>assets2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url() ?>assets2/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="<?php echo base_url() ?>assets2/css/stylish-portfolio.css" rel="stylesheet">

    <!--para las notificaciones-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/plugins/jquery.growl/css/jquery.growl.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets2/css/estilos.css">


</head>

<body id="page-top">

<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">

  <!-- Navigation -->
  <!--<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top">Start Bootstrap</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#page-top">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#about">About</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#services">Services</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="<?php echo base_url();?>Login">Login</a>
      </li>
    </ul>
  </nav>-->

  <!-- Header -->
  <header class="masthead d-flex">
    <div class="container text-center my-auto">

      <img src="<?php echo base_url(); ?>assets2/img/logo.png" alt="Logo">

      <h1 class="mb-1 mt-3">Crea tu usuario Ecobot</h1>
      <h3 class="mb-5">
        <em>y accede a grandes beneficios</em>
      </h3>
      <a class="btn btn-primary btn-xl js-scroll-trigger mx-2 mb-2" href="#about">Empezar</a>
      <a class="btn btn-primary btn-xl js-scroll-trigger mx-2 mb-2" href="<?php echo base_url();?>">Inicio</a>
      <!--<a class="btn btn-primary btn-xl" href="<?php echo base_url();?>Login">Ver mis botellas</a>-->
    </div>
    <div class="overlay"></div>
  </header>

  <!-- About -->
  <section class="content-section bg-light" id="about">
    <div class="container ">
        <div class="row text-center">
            <div class="col-xs-12 col-lg-10 mx-auto">
                <h2>Crea tu usuario Ecobot</h2>
                <p id="parrafo" class="lead mb-5">Los datos que ingreses serán los que usaremos para contactarte cuando ganes</p>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-lg-10 mx-auto">

                <form id="formRegistroIndependiente"  >
                
                    <div class="row">
                        <div class="col-md-7 my-2">
                            <label for="tipoDocumento2">Tipo Documento</label>
                            <select class="form-control" name="tipoDocumento2" id="tipoDocumento2">
                                <option value="">Selecciona tipo de documento</option>
                                <option value="1">Cédula de Ciudadanía</option>
                                <option value="2">Tarjeta de Identidad</option>
                                <option value="3">Pasaporte</option>
                                <option value="4">Cédula de Extranjería</option>
                            </select>

                        </div>
                        <div class="col-md-5 my-2">
                            <label for="numeroDocumento2">N&uacute;mero de Documento</label>
                            <input type="number" class="form-control" name="numeroDocumento2" id="numeroDocumento2" onkeypress="return valideKey(event);">
                        </div>

                        <div class="col-md-6 my-2">
                            <label for="nombreCompleto2">Nombre y apellido</label>
                            <input type="text" class="form-control" name="nombreCompleto2" id="nombreCompleto2" >
                        </div>

                        <div class="col-md-6 my-2">
                            <label for="tlfContacto2">Celular de contacto</label>
                            <input type="number" class="form-control" name="tlfContacto2" id="tlfContacto2" >
                        </div>

                        <div class="col-md-4 my-2">
                            <label for="email2">Correo</label>
                            <input type="email" class="form-control" name="email2" id="email2" >
                        </div>
                        <div class="col-md-4 my-2">
                            <label for="pass2">Crea una contraseña</label>
                            <input type="password" class="form-control" name="pass2" id="pass2" >
                        </div>

                        <div class="col-md-4 my-2">
                            <label for="cuidad2">Ciudad donde reciclas</label>
                            <select class="form-control" name="cuidad2" id="cuidad2">
                                <option value="">Selecciona tu ciudad</option>
                                <option value="bogota">Bogotá y alrededores</option>
                                <option value="medellin">Medellín</option>
                                <option value="cali">Cali</option>
                                <option value="barranquilla">Barranquilla</option>
                            </select>
                        </div>

                    </div>

                    <div class="row mt-2 text-center">
                        <div class="col-md-12">
                            <button type="submit" id="btnGuardarUsuarioIndependiente"  class="btn btn-success" >Registrarme</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
            
    </div>


  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">

        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="https://www.instagram.com/soyecobot/">
            <i class="icon-social-instagram"></i>
          </a>
        </li>

        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="https://www.facebook.com/soyecobot">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="https://twitter.com/soyecobot">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
        
      </ul>
      <p class="text-muted small mb-0">Copyright &copy; Your Website 2020</p>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url() ?>assets2/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url() ?>assets2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url() ?>assets2/js/stylish-portfolio.min.js"></script>

  <!--script de notificaciones-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/plugins/jquery.growl/js/jquery.growl.js"></script>

  <script src="<?php echo base_url() ?>assets/plugins/jquery-form-validation/jquery.validate.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

  <script type="text/javascript" src="<?php  echo base_url() ?>assets2/js/global/global.js"></script>

  <script>

        var input=  document.getElementById('numeroDocumento2');

        input.addEventListener('input',function(){
            if (this.value.length > 12) 
                this.value = this.value.slice(0,12); 
        })

        function valideKey(evt) {
            var code = evt.which ? evt.which : evt.keyCode;

            if (code == 8) {
                //backspace
                return true;
            } else if (code >= 48 && code <= 57) {
                //is a number
                return true;
            } else {
                return false;
            }
        }

        //registro independiente
        $("#formRegistroIndependiente").validate({
                rules: {
                    tipoDocumento2:{
                        required: true
                    },
                    numeroDocumento2:{
                        required: true,
                    },
                    nombreCompleto2: {
                        required: true
                    },
                    email2: {
                        required: true,
                        email: true
                    },
                    pass2: {
                        required: true
                    },
                    tlfContacto2: {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    cuidad2: {
                        required: true
                    }
                },
                messages: {
                    tipoDocumento2:{
                        required: "Campo Obligatorio"
                    },
                    numeroDocumento2:{
                        required: "Campo Obligatorio"
                    },
                    nombreCompleto2: {
                        required: "Campo Obligatorio"
                    },
                    email2: {
                        required: "Campo Obligatorio",
                        email: "tiene que ser un email valido"
                    },
                    pass2: {
                        required: "Campo Obligatorio"
                    },
                    tlfContacto2: {
                        required: "Campo Obligatorio",
                        minlength: "Tienes que ingresar mínimo 10 números",
                        maxlength:  "Tienes que ingresar máximo 10 números"
                    },
                    cuidad2: {
                        required: "Campo Obligatorio"
                    }
                },
                submitHandler: function(form){
                    var base_url = $('#base_url').val();
                    
                    $.ajax({

                        url: base_url +"Registro/guardarRegistro",
                        type: "POST",
                        dataType: "html",
                        data: $("#formRegistroIndependiente").serialize()
                        
                    }).done(function(data){
                
                        var response = $.parseJSON(data);
                        
                        if(response.code == 1){
                            //$.growl.notice({title: "Hola", message: response.mensaje, duration: 3000 });
                            //setTimeout(redirectIndex,3000);
                            //msjConfirmacin('¡Gracias por registrarte!',response.mensaje); 

                            Swal.fire({
                                title: '¡Gracias por registrarte!',
                                text: response.mensaje,
                                icon: 'success',
                                confirmButtonText: 'OK'
                            }).then((result) => {
                                if (result.isConfirmed) {

                                  var base_url = $("#base_url").val();

                                  location.href= base_url;
                                }
                            })
                        }
                        if(response.code != 1){
                            //$.growl.error({title: "Error", message: response.mensaje, duration: 3000 });
                            //$("#formPerfil")[0].reset();
                            msjError(response.mensaje);
                        }
                        
                    }).fail(function() {
                            alert( "error" );
                        })
                    .always(function() {
                    });
                }
            });

  </script>

</body>

</html>
