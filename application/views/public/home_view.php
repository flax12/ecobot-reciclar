<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Stylish Portfolio - Start Bootstrap Template</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?php echo base_url() ?>assets2/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="<?php echo base_url() ?>assets2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url() ?>assets2/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="<?php echo base_url() ?>assets2/css/stylish-portfolio.css" rel="stylesheet">

    <!--para las notificaciones-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/plugins/jquery.growl/css/jquery.growl.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets2/css/estilos.css">


</head>

<body id="page-top">

<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">

  <!-- Navigation -->
  <!--<a class="menu-toggle rounded" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
      <li class="sidebar-brand">
        <a class="js-scroll-trigger" href="#page-top">Start Bootstrap</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#page-top">Home</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#about">About</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#services">Services</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="#portfolio">Portfolio</a>
      </li>
      <li class="sidebar-nav-item">
        <a class="js-scroll-trigger" href="<?php echo base_url();?>Login">Login</a>
      </li>
    </ul>
  </nav>-->

  <!-- Header -->
  <header class="masthead d-flex">
    <div class="container text-center my-auto">

       <img src="<?php echo base_url(); ?>assets2/img/logo.png" alt="Logo">


      <h1 class="mb-1 mt-3">Registra tus cupones</h1>
      <h3 class="mb-5">
        <em>y accede a grandes beneficios</em>
      </h3>
      <a onclick="registrarme()"  class="btn btn-primary btn-xl js-scroll-trigger mx-2 mb-2" href="#about">Registrar códigos</a>
      <a  class="btn btn-primary btn-xl js-scroll-trigger mx-2 mb-2" href="<?php echo base_url()?>Registro">Crear cuenta</a>
      <a class="btn btn-primary btn-xl mx-2" href="<?php echo base_url();?>Login">Ver mi histórico</a>
    </div>
    <div class="overlay"></div>
  </header>

  <!-- About -->
  <section class="content-section bg-light" id="about">
    <div class="container ">
        <div class="row text-center">
            <div class="col-xs-12 col-lg-10 mx-auto">
                <h2 id="titulo">Registra tus cupones</h2>
                <p id="parrafo" class="lead mb-5">
                    Ingresa tu tipo y número de documento de identidad, 
                    seguido de la cantidad de cupones que vas a registrar.
                     Una vez ingreses tus códigos, le das enviar y listo
                </p>
            </div>

            <div class="col-xs-12 col-lg-10 mx-auto">
                <form id="formRegistroCupones">
                    <div class="row">
                        <div class="col-md-6 my-2">
                            <label for="tipoDocumentoCupon">Tipo Documento</label>
                            <select class="form-control" name="tipoDocumentoCupon" id="tipoDocumentoCupon">
                                <option value="">Selecciona tipo de documento</option>
                                <option value="1">Cédula de Ciudadanía</option>
                                <option value="2">Tarjeta de Identidad</option>
                                <option value="3">Pasaporte</option>
                                <option value="4">Cédula de Extranjería</option>
                            </select>

                        </div>
                        <div class="col-md-6 my-2">
                            <label for="numeroDocumentoCupon">N&uacute;mero de Documento</label>
                            <input type="number" class="form-control" name="numeroDocumentoCupon" id="numeroDocumentoCupon" onkeypress="return valideKey(event);">
                        </div>

                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-3 col-md-3 my-2">
                            <label for="numeroCuponesIngresar">Cantidad de Cupones</label>
                            <input  type="number" class="form-control" name="numeroCuponesIngresar" id="numeroCuponesIngresar" />
                        </div>
                        <div class="col-xs-3 col-md-3 my-2" style="padding-top:31px;">
                            <button type="button" id="agregarCamposDeCupones" class="btn btn-primary" onclick="agregarCampos()">Ingresar códigos</button>
                        </div>
                        <div class="col-md-3 my-2" >

                        </div>
                    </div>

                    <div id="camposDinamicos" class="my-2">

                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <button type="button" id="btnGuardarCupon"  class="btn btn-lg btn-success" >Enviar</button>
                        </div>
                    </div>
                    
                    
                </form>

            </div>
      </div>

        <div class="row">
            <div class="col-xs-12 col-lg-10 mx-auto">

              <form id="formRegistroUsuario" style="display:none;" >
                    
                    <input type="hidden" name="tipoDocumento" id="tipoDocumento">
                    <input type="hidden" name="numeroDocumento" id="numeroDocumento">
                  
                  <div class="row">

                        <div class="col-md-6 my-2">
                            <label for="nombreCompleto">Nombres y Apellidos</label>
                            <input type="text" class="form-control" name="nombreCompleto" id="nombreCompleto" >
                        </div>

                        <div class="col-md-6 my-2">
                            <label for="tlfContacto">Celular de contacto</label>
                            <input type="number" class="form-control" name="tlfContacto" id="tlfContacto" >
                        </div>

                        <div class="col-md-4 my-2">
                            <label for="email">Correo</label>
                            <input type="email" class="form-control" name="email" id="email" >
                        </div>
                        <div class="col-md-4 my-2">
                            <label for="pass">Crea una contraseña</label>
                            <input type="password" class="form-control" name="pass" id="pass" >
                        </div>

                        <div class="col-md-4 my-2">
                            <label for="cuidad">Ciudad donde reciclas</label>
                            <select class="form-control" name="cuidad" id="cuidad">
                                <option value="">Selecciona tu ciudad</option>
                                <option value="bogota">Bogotá y alrededores</option>
                                <option value="medellin">Medellín</option>
                                <option value="cali">Cali</option>
                                <option value="barranquilla">Barranquilla</option>
                            </select>
                        </div>

                    </div>

                    <div class="row mt-2 text-center">
                        <div class="col-md-12">
                            <button type="submit" id="btnGuardarUsuario"  class="btn btn-success" >Registrar</button>
                        </div>
                    </div>

                </form>

             </div>
        </div>

    </div>

  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="https://www.instagram.com/soyecobot/">
            <i class="icon-social-instagram"></i>
          </a>
        </li>

        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="https://www.facebook.com/soyecobot">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="https://twitter.com/soyecobot">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
        
      </ul>
      <p class="text-muted small mb-0">Copyright &copy; Your Website 2020</p>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url() ?>assets2/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url() ?>assets2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url() ?>assets2/js/stylish-portfolio.min.js"></script>

  <!--script de notificaciones-->
  <script type="text/javascript" src="<?php echo base_url()?>assets/plugins/jquery.growl/js/jquery.growl.js"></script>

  <script src="<?php echo base_url() ?>assets/plugins/jquery-form-validation/jquery.validate.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  
  <script type="text/javascript" src="<?php  echo base_url() ?>assets2/js/global/global.js"></script>

  <script>

        function registrarme(opcion){

            $("#formRegistroCupones").fadeIn(2000);
            $("#formRegistroUsuario").fadeOut();

            $("#titulo").text('Registra tus cupones');
            $("#parrafo").text('Ingresa tu tipo y número de documento de identidad, seguido de la cantidad de cupones que vas a registrar. Una vez ingreses tus códigos, le das enviar y listo');

        }

        var input=  document.getElementById('numeroDocumento');

        input.addEventListener('input',function(){
            if (this.value.length > 12) 
                this.value = this.value.slice(0,12); 
        })

        function valideKey(evt) {
            var code = evt.which ? evt.which : evt.keyCode;

            if (code == 8) {
                //backspace
                return true;
            } else if (code >= 48 && code <= 57) {
                //is a number
                return true;
            } else {
                return false;
            }
        }

        function agregarCampos(){
            var cantidad = $("#numeroCuponesIngresar").val();

            var input = '';

            for (let index = 0; index < cantidad; index++) {
                
                input += '<div class="row my-1"><div class="col-md-4"><input class="form-control" type="text" name="cupon[]" placeholder="Ingrese cupon"/></div></div>';
                
            }

            $("#camposDinamicos").html(input);
        }

        //funcion encargada de mostrar el formulario de registro
        function mostrarFormRegistro(){
            //document.getElementById('formRegistroUsuario').style.display = "";
            //document.getElementById('formRegistroCupones').style.display = "none";
            $("#formRegistroCupones").fadeOut();
            $("#formRegistroUsuario").fadeIn(2000);

        }
        
        

        $(document).ready(function(){
   
            $("#btnGuardarCupon").click(function(){
                var base_url = $("#base_url").val();

                $.ajax({
                    type: "POST",
                    url: base_url + "Publico/recibirCupones",
                    data: $("#formRegistroCupones").serialize(),
                    success: function(respuesta){
                        var response = $.parseJSON(respuesta);
                        

                        if (response.code == 0) {
                            //$.growl.error({title: "Hola", message: response.mensaje, duration: 3000 });
                            msjError(response.mensaje);
                        }

                        if( response.code == 300) {
                            Swal.fire({
                                icon: 'error',
                                title: response.mensaje,
                                text: ''
                            })
                        }

                        if( response.code == 305) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops!',
                                text: response.mensaje
                            })
                        }

                        if(response.code == 404){
                            
                            Swal.fire({
                                title: 'Espera, no te has registrado',
                                text: response.mensaje,
                                icon: 'error',
                                confirmButtonText: 'Registrame'
                            }).then((result) => {
                                if (result.isConfirmed) {

                                    $("#titulo").text('Crea tu usuario Ecobot');
                                    $("#parrafo").text('Los datos que ingreses serán los que usaremos para contactarte cuando ganes');


                                    var tipoDocumento = $("#tipoDocumentoCupon").val();
                                    var numeroDocumento = $("#numeroDocumentoCupon").val();

                                    setTimeout(mostrarFormRegistro,500);

                                    $("#tipoDocumento").val(tipoDocumento);
                                    $("#numeroDocumento").val(numeroDocumento);
                                    
                                }
                            })

                        }
                        
                        if(response.code == 1){
                            //$.growl.notice({title: "Hola", message: response.mensaje, duration: 3000 });
                            //setTimeout(redirectIndex,3000);
                            msjConfirmacin('¡Listo!',response.mensaje);
                        }
                        
                    },
                    dataType: "html"
                });
            });


            var base_url = $('#base_url').val();

            $("#formRegistroUsuario").validate({
                rules: {
                    tipoDocumento:{
                        required: true
                    },
                    numeroDocumento:{
                        required: true,
                    },
                    nombreCompleto: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    pass: {
                        required: true
                    },
                    tlfContacto: {
                        required: true,
                        minlength: 10,
                        maxlength: 10
                    },
                    cuidad: {
                        required: true
                    }
                },
                messages: {
                    tipoDocumento:{
                        required: "Campo Obligatorio"
                    },
                    numeroDocumento:{
                        required: "Campo Obligatorio"
                    },
                    nombreCompleto: {
                        required: "Campo Obligatorio"
                    },
                    email: {
                        required: "Campo Obligatorio",
                        email: "tiene que ser un email valido"
                    },
                    pass: {
                        required: "Campo Obligatorio"
                    },
                    tlfContacto: {
                        required: "Campo Obligatorio",
                        minlength: "Tienes que ingresar mínimo 10 números",
                        maxlength:  "Tienes que ingresar máximo 10 números"
                    },
                    cuidad: {
                        required: "Campo Obligatorio"
                    }
                },
                submitHandler: function(form){
                    
                    $.ajax({

                        url: base_url + "Publico/registrarUsuarios",
                        type: "POST",
                        dataType: "html",
                        data: $("#formRegistroUsuario").serialize()
                        
                    }).done(function(data){
                
                        var response = $.parseJSON(data);
                        
                        if(response.code == 1){
                            //$.growl.notice({title: "Hola", message: response.mensaje, duration: 3000 });
                            //setTimeout(redirectIndex,3000);
                            msjConfirmacin('¡Gracias por registrarte!',response.mensaje); 
                        }
                        if(response.code != 1){
                            //$.growl.error({title: "Error", message: response.mensaje, duration: 3000 });
                            //$("#formPerfil")[0].reset();
                            msjError(response.mensaje);
                        }
                        
                    }).fail(function() {
                            alert( "error" );
                        })
                    .always(function() {
                    });
                }
            });
        });

  </script>

</body>

</html>
