<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login ecobot</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/adminlte.min.css">

  <!--para las notificaciones-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/plugins/jquery.growl/css/jquery.growl.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
  <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
<div class="login-box">
  <div class="login-logo">
  <img width="80" height="70" src="<?php echo base_url(); ?>assets2/img/logo.png" alt="Logo">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicia sesión para ver tu histórico</p>

      <form id="formLogin">
        <div class="input-group mb-3">
          <input type="email" name="email" id="email" class="form-control" placeholder="Correo">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="pass" name="pass" class="form-control" placeholder="Contraseña">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
       
          <!-- /.col -->
          <div class="col-12">
            <button type="button" id="btnLogin" class="btn btn-primary btn-block">Iniciar sesión</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!-- /.social-auth-links -->

      <p class="mb-1 ml-2">
        ¿Olvidaste tu contraseña? Envíanos tus datos a <a href="mailto:ecobot@ag-corporation.com">ecobot@ag-corporation.com</a> y te ayudaremos a recuperarla
      </p>
      <!--<p class="mb-0 pl-3">
        <a href="<?php echo base_url();?>Registro" class="text-center">Registrarme</a>
      </p>-->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/js/adminlte.min.js"></script>

<!--script de notificaciones-->
<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/jquery.growl/js/jquery.growl.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<!--Login script-->
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/login/login.js"></script>

</body>
</html>
