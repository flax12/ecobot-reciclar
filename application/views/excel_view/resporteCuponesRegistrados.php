<?php 
header('Content-type:application/xls');
header('Content-Disposition: attachment; filename=reporte.xls');

?>
<table style="border: 1px solid black">
	<tr style="border: 1px solid black">
        <th>ID</th>
        <th>Nombre Completo</th>
        <th>Correo</th>
        <th>Tipo de documento</th>
        <th>Numero de documento</th>
        <th>Ciudad</th>
        <th>Celular</th>
        <th>Cupon</th>
        <th>fecha de Registro Cupon</th>
	</tr>
    <?php  foreach ($data as $value) { ?>

	<tr style="border: 1px solid black">
        <td><?php echo $value['id_cuponesUsuarios'] ?></td>
        <td><?php echo $value['nombreCompleto'] ?></td>
        <td><?php echo $value['email'] ?></td>
        <td>
            <?php  
                    switch ($value['tipoDocumento']) {
                        case 1:
                            echo "Cédula de Ciudadanía";
                            break;
                        case 2:
                            echo "Tarjeta de Identidad";
                            break;
                        case 3:
                            echo "Pasaporte";
                            break;
                              
                        default:
                            echo "Cédula de Extranjería";
                            break;
                    }
                ?>
        </td>
                     
        <td><?php echo $value['numeroDocumento'] ?></td>
        <td><?php echo $value['cuidad'] ?></td>
        <td><?php echo $value['tlfContacto'] ?></td>
        <td><?php echo $value['cupon'] ?></td>
        <td><?php echo $value['fechaRegistro'];?></td>
	</tr>

<?php } ?>


</table>