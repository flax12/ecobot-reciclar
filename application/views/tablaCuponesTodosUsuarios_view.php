<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listado de Cupones registrado</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()."Panel" ?>">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado</h3><br>
              <a target="blank" class="btn btn-success" href="<?php echo base_url()."Panel/excelCuponesRegistradosUsuarios" ?>" role="button">Exportar excel</a>
            </div>
            
            <!-- /.card-header -->
            <div class="table-responsive card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre Completo</th>
                  <th>Correo</th>
                  <th>Tipo de documento</th>
                  <th>Numero de documento</th>
                  <th>Ciudad</th>
                  <th>Celular</th>
                  <th>Cupon</th>
                  <th>fecha de Registro Cupon</th>
                </tr>
                </thead>
                <tbody>

                <?php 

                  $botellas = 0;

                  foreach($data as $value ) { 
                  
                  $botellas += 1;
                
                ?>
                  <tr>

                      <td><?php echo $value['id_cuponesUsuarios'] ?></td>
                      <td><?php echo $value['nombreCompleto'] ?></td>
                      <td><?php echo $value['email'] ?></td>

                      <td>
                        <?php  

                            switch ($value['tipoDocumento']) {
                                case 1:
                                    echo "Cédula de Ciudadanía";
                                    break;
                                case 2:
                                    echo "Tarjeta de Identidad";
                                    break;
                                case 3:
                                    echo "Pasaporte";
                                    break;
                              
                                default:
                                    echo "Cédula de Extranjería";
                                    break;
                            }
                        ?>
                      </td>
                     
                      <td><?php echo $value['numeroDocumento'] ?></td>
                      <td><?php echo $value['cuidad'] ?></td>

                      <td><?php echo $value['tlfContacto'] ?></td>

                      
                      <td><?php echo $value['cupon'] ?></td>
                      <td><?php echo $value['fechaRegistro'];?></td>

                  </tr>
                <?php } ?>
                  
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>


<?php 
	$this->load->view('templates/footer'); 
?>