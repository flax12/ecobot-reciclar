<?php 
  $this->load->view('templates/header'); 
  $this->load->view('templates/menu'); 
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Listado de Usuarios</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url()."Panel" ?>">Home</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listado</h3>
              <br>
              <a target="blank" class="btn btn-success" href="<?php echo base_url()."Panel/excelUsuarios" ?>" role="button">Exportar excel</a>
            </div>
            
            <!-- /.card-header -->
            <div class="table-responsive card-body">

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre Completo</th>
                  <th>Tipo Documento</th>
                  <th>N&uacute;mero Documento</th>
                  <th>Correo</th>
                  <th>Estado</th>
                  <th>Detalles</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($data as $value ) { 
                  
                  $datos = $value['tlfContacto'].'||'.$value['cuidad'];
                
                ?>
                  <tr>

                      <td><?php echo $value['id_users'] ?></td>
                      <td><?php echo $value['nombreCompleto'] ?></td>

                      <td>
                        <?php  

                            switch ($value['tipoDocumento']) {
                                case 1:
                                    echo "Cédula de Ciudadanía";
                                    break;
                                case 2:
                                    echo "Tarjeta de Identidad";
                                    break;
                                case 3:
                                    echo "Pasaporte";
                                    break;
                              
                                default:
                                    echo "Cédula de Extranjería";
                                    break;
                            }
                        ?>
                      </td>
                      <td><?php  echo $value['numeroDocumento'];?></td>

                      <td><?php  echo $value['email'];?></td>

                      <td>
                      <?php 
                            if ($value['estado'] == 1) {
                                echo  "ACTIVO";
                            }else{
                                echo  "INCACTIVO";
                            }
                        ?>    
        
                      </td>

                      <td>
                        <button  onclick="infoAModalUsuario('<?php echo $datos; ?>')" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEditar">
                            detalles
                        </button>
                      </td>

                      <td>
                            <a class="btn btn-info" href="<?php echo base_url().'Usuario/editar/'.$value['id_users']; ?>"> Editar</a>
                      </td>

                      <td>
                            
                         <button  onclick="pasarIdUsuario(<?php echo $value['id_users']; ?>)" type="button" class="btn btn-danger" >
                            Eliminar
                        </button>

                      </td>


                  </tr>
                <?php } ?>
                  
                </tbody>

              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Cupon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <h4>Deseas eliminar este Usuario?</h4>
        <form id="formEliminarUser">
            <input type="hidden" name="id_users" id="id_users">
            <button type="button" id="btnEliminarUser" class="btn btn-success">SI</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!--modal editar-->
<div class="modal fade" id="modalEditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">

            <div class="container">
               
                <div class="row">
                    
                    <div class="col-md-12">
                        <p id="tlf"></p>
                    </div>
                    <div class="col-md-12">
                        <p id="cuidad"></p>
                    </div>
        
                </div>
            
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

  </div>

<?php 
	$this->load->view('templates/footer'); 
?>