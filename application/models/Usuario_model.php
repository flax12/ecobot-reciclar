<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{

    public function __construct() 
	  {
		  parent::__construct();
    }

    public function insertUsuario($credenciales, $data){
        $db = $this->db;
        $db->insert('users',$credenciales);
        $idUser =  $db->insert_id();

        unset($data['email']);
        unset($data['pass']);

        $data = array('id_users' => $idUser)+$data;

        $db->insert('informacion',$data);
        $idUserinfo =  $db->insert_id();

        return $idUser;
    }

    public function validarEmail($email){
      $db = $this->db;
      $db->where('email', $email);
      $data = $db->get('users')->result_array();
      return $data;
    }

    //validar id del registro
    public function validarIdRegistro($idRegistro){
      $db = $this->db;
      $db->where('id_information', $idRegistro);
      $data = $db->get('information')->result_array();
      return $data;
    }

    public function validarUsuario($usuraio, $pass){

      $db = $this->db;     
      $db->select("*");
      $db->from("users u");
      $db->join("informacion inf", "inf.id_users = u.id_users");
      $db->where('u.email', $usuraio);
      $db->where("u.pass", $pass);

      return $db->get()->result_array();
    }

    public function validarUsuarioPorDocumento($tipoDocumento,$numero){
      $db = $this->db;
      $db->where('tipoDocumento', $tipoDocumento);
      $db->where('numeroDocumento', $numero);
      $data = $db->get('informacion')->result_array();
      return $data;
    }


    //validar que el hash sea del usario correcto
    public function existsUser($idUser,$hash){
      $db = $this->db;

      $db->where("id_users", $idUser);
      $db->where("hashVerificacion", $hash);

      $data = $db->get('users')->result_array();
      return $data;
    }

    //actualizar Estatus
    public function actualizarStatus($id){
      $db = $this->db;
      $insert = array(
        'status' => 1,
        "hashVerificacion" => ""
      );

      $db->where('id_users', $id);
      $db->update("users",$insert);
    }

    //get de los registros realizados
    public function getRegistros(){

      $db = $this->db;     
      $db->select("*");
      $db->from("users u");
      $db->join("informacion inf", "inf.id_users = u.id_users");
      $db->where('u.rol', 1);

      return $db->get()->result_array();
    }

    public function  updateStatusRegister($id_users){
      $db = $this->db;
      $array = array(
        'status' => 1
      );

      $db->update('users',$array);
    }


    public function deleteRegistros($id_users){
      $db = $this->db;

      $db->delete('information',array('id_users' => $id_users));
      $db->delete('users',array('id_users' => $id_users));

    }

    public function dataInfoUser($id){
      $db = $this->db;     
      $db->select("*");
      $db->from("users u");
      $db->join("informacion inf", "inf.id_users = u.id_users");
      $db->where('u.id_users', $id);
      return $db->get()->result_array();
    }

    //update info
    public function actualizarUser($id,$credenciales,$info){
      $db = $this->db;
      
      $db->where('id_users', $id);
      $db->update("users",$credenciales);

      $db->where('id_users', $id);
      $db->update("informacion",$info);
      
    }

    //seccion Cupon
      public function insertCupon($data) {
        $db = $this->db;
        $db->insert('cupones',$data);
        return $db->insert_id();
      }

      public function validarCupon($cupon){
        $db = $this->db;
        $db->where('cupon', $cupon);
        return $db->get('cupones')->result_array();
      }

      public function getCupones(){
        return $this->db->get('cupones')->result_array();
      }

      public function eleminarCupon($idCupon){
        $this->db->delete('cupones', array('id_cupones' => $idCupon));
      }

      public function updateCupon($id,$update){
        $db = $this->db;
        
        $db->where('id_cupones', $id);
        $db->update("cupones",$update);
      }

      public function usuariosMesCupones($idUser,$mes){
        $db = $this->db;
        $db->where('id_users', $idUser);
        $db->where('mes', $mes);
        return $db->get('cuponesusuarios')->result_array();
      }

      public function usuariosCuponesTodos($idUser){
        $db = $this->db;
        $db->where('id_users', $idUser);
        return $db->get('cuponesusuarios')->result_array();
      }
    //end cupon

    public function deleteUsuario($data) {
      $this->db->delete('users', $data);
    }

    public function insertCuponesUsuarios($data){
      $db = $this->db;
      $db->insert('cuponesusuarios',$data);
      return $db->insert_id();
    }

    public function getCuponesUser($idUser){
      $db = $this->db;
      $db->where("id_users", $idUser);
      return $db->get('cuponesusuarios')->result_array();
     
    }

    public function getCuponesTodoUser(){
        $db = $this->db;    
        $db->select("*");
        $db->from("cuponesusuarios c");
        $db->join("users u", "u.id_users = c.id_users");
        $db->join("informacion inf", "inf.id_users = c.id_users");
        $db->where('u.rol', 1);
        return $db->get()->result_array();
    }


}  

/*End of file Usuario_model.php*/  