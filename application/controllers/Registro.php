<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Usuario_model");
        $this->load->library("send");

	}


    public function guardarRegistro(){

        $data = $this->input->post(null,true);

        if(!isset($data['tipoDocumento2']) || empty($data['tipoDocumento2'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que selecionar el tipo de documento"
            );
            echo json_encode($response);
            return;
		}
		
		if(!isset($data['numeroDocumento2']) || empty($data['numeroDocumento2'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que ingresar el numero de documento"
            );
            echo json_encode($response);
            return;
        }
        
        if(!isset($data['nombreCompleto2']) || empty($data['nombreCompleto2'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que ingresar el tu nombre completo no pùede estar vacio el campo."
            );
            echo json_encode($response);
            return;
        }

        if(!isset($data["email2"]) || empty($data["email2"]) || !filter_var($data["email2"], FILTER_VALIDATE_EMAIL) ){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "El correo no es valido"
            );
            echo json_encode($response);
            return;
        }
            
        if(!isset($data["pass2"]) || empty($data["pass2"])){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "password inválido, o  no puede estar vacio"
            );
            echo json_encode($response);
            return;
        }

        if(!isset($data['tlfContacto2']) || empty($data['tlfContacto2'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tienes que ingresar el teléfono de contacto"
            );
            echo json_encode($response);
            return;
        }
        
        if(!isset($data['cuidad2']) || empty($data['cuidad2'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tienes que ingresar LA CIUDAD"
            );
            echo json_encode($response);
            return;
        }

        $existeUsuario = $this->Usuario_model->validarUsuarioPorDocumento($data['tipoDocumento2'],$data['numeroDocumento2']);

		if(isset($existeUsuario) && count($existeUsuario) > 0 ){
			$response = array(
                "code"    =>    0,
                "mensaje" =>    "Este documento de identidad ya ha sido registrado antes. Puedes usarlo para ingresar tus códigos."
            );
            echo json_encode($response);
            return;
        }

        //creo el hash para verificar el email de los usuarios
        //$hash = $this->crearHash();

        $credenciales = array(
            'email' => $data['email2'],
            'pass' => md5($data['pass2']),
            'rol' => 1,
            'estado' => 1
        );

        $infoUsuario = array(
            'tipoDocumento' => $data['tipoDocumento2'],
            'numeroDocumento' => $data['numeroDocumento2'], 
            'nombreCompleto' => $data['nombreCompleto2'],
            'tlfContacto' => $data['tlfContacto2'], 
            'cuidad' => $data['cuidad2'], 
        );

        //validos si existe el email en base de datos

        $existeEmail = $this->Usuario_model->validarEmail($data['email2']);

        if (isset($existeEmail) && !empty($existeEmail)) {
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Este correo ya ha sido registrado antes. Solo puedes crear una cuenta por correo."
            );
            echo json_encode($response);
            return;
        }

        $idUsuario = $this->Usuario_model->insertUsuario($credenciales, $infoUsuario);

        if(isset($idUsuario) && $idUsuario > 0){
            $response = array(
                "code"    =>    1,
                "mensaje" => "Ya puedes empezar a registrar tus cupones" 
            );
            echo json_encode($response);
            return;

        }else {

            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Error al registrar al usuario"
            );
            echo json_encode($response);
            return;

        }

        
    }

    private function crearHash() {

        $length = 32;

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    } 

    public function activacionCuenta($idUser = null, $hash = null){

        if ($idUser != null && $hash != null) {

            $existeUser = $this->Usuario_model->existsUser($idUser,$hash);

            if (count($existeUser)) {

                $this->session->set_userdata("isLoggedIn", true);
                $this->session->set_userdata("id_usuario", $resultado[0]["id_users"]);
                $this->session->set_userdata("email", $resultado[0]["email"]);
                $this->session->set_userdata("rol", $resultado[0]["rol"]); 
                $this->session->set_userdata("nombres", $resultado[0]["nombres"]);

                $this->Usuario_model->actualizarStatus($existeUser[0]["id_users"]);

                redirect("panel");

            }else {
                $data= array(
                    'title' => "Error"
                );

                echo "<h1>Error 404</h1>";
                //$this->load->view('error_view',$data);
            }
        }else{
            $data= array(
                'title' => "Error"
            );
            echo "<h1>Error 404</h1>";
            //$this->load->view('error_view',$data);
        }
    }
}

/*End of file Registro.php*/
