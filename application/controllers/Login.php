<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Usuario_model");

		$session = $this->session->all_userdata();

        //print_r($session);
        if(isset($session["isLoggedIn"]) && $session["id_usuario"] > 0){ 
            redirect("panel");
        }	
	}

	public function index()
	{
		$data = array(
			'title' => "Document"
		);
		$this->load->view('public/login_view',$data);
	}

	//metodo para inicar session
	public function iniciarSesion(){

		$data = $this->input->post(null,true);

		//var_dump($data);

		if(!isset($data["email"]) || empty($data["email"]) || !filter_var($data["email"], FILTER_VALIDATE_EMAIL) ){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "usuario invalido"
            );
            echo json_encode($response);
            return;
        }
            
        if(!isset($data["pass"]) || empty($data["pass"])){
            $response = array(
                "code"    =>    200,
                "mensaje" =>    "password inválido"
            );
            echo json_encode($response);
            return;
        }

    
        $usuario = $data["email"];
        $password = md5($data["pass"]);

        $resultado = $this->Usuario_model->validarUsuario($usuario, $password);

        if(count($resultado) > 0){

            //valido que estatus de usuarios es y el estatus
            //valido que estatus de usuarios es y el estatus
            if ($resultado[0]["estado"] == 1) {

                $this->session->set_userdata("isLoggedIn", true);
                $this->session->set_userdata("id_usuario", $resultado[0]["id_users"]);
                $this->session->set_userdata("email", $resultado[0]["email"]);
                $this->session->set_userdata("rol", $resultado[0]["rol"]); 
                $this->session->set_userdata("nombres", $resultado[0]["nombreCompleto"]); 
                //_________________________________
                            
                $response = array(
                    "code"    =>  1,
                    "mensaje" =>    "Inicio de sesión correcto."
                );
                echo json_encode($response);
                return;
                
            }else{
                $response = array(
                    "code"    =>  0,
                    "mensaje" =>    "Tienes que confirmar tu correo para poder acceder al sistema gracias!"
                );
                echo json_encode($response);
                return;
            }
        
                
        }else{
            $response = array(
              "code"    =>  300,
              "mensaje" =>    "Usuario o password invalido."
            );
            echo json_encode($response);
            return;
        }

	}


}

/*End of file Login.php*/
