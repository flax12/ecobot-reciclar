<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publico extends CI_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model("Usuario_model");
	}

	public function home(){
		$this->load->view('public/home_view');
    }
    
    public function registro(){
        $this->load->view('public/registro_view');
    }

	public function recibirCupones(){
        $data = $this->input->post(null,true);
        
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

		if(!isset($data['tipoDocumentoCupon']) || empty($data['tipoDocumentoCupon'])){
            $response = array(
                "code"    =>    300,
                "mensaje" =>    'Debes seleccionar tu tipo de documento'
            );
            echo json_encode($response);
            return;
		}
		
		if(!isset($data['numeroDocumentoCupon']) || empty($data['numeroDocumentoCupon'])){
            $response = array(
                "code"    =>    300,
                "mensaje" =>    "Debes ingresar tu número de documento"
            );
            echo json_encode($response);
            return;
		}
		
		if(!isset($data['cupon']) || empty($data['cupon'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Tienes que ingresar al menos 1 cupon , intenta nevamente"
            );
            echo json_encode($response);
            return;
        }

        $this->session->set_userdata("cupones", $data['cupon']);

		$existeUsuario = $this->Usuario_model->validarUsuarioPorDocumento($data['tipoDocumentoCupon'],$data['numeroDocumentoCupon']);

		if(!isset($existeUsuario) || count($existeUsuario) == 0 ){
			$response = array(
                "code"    =>    404,
                "mensaje" =>    "Debes crear una cuenta para poder contactarte. No te preocupes, los códigos que ingresaste se guardarán en tu cuenta"
            );
            echo json_encode($response);
            return;
        }

        $guardados = $this->validarCupones($existeUsuario[0]['id_users']);

        $noGuardados = count($data['cupon']) - $guardados;

        $session = $this->session->all_userdata();
        $cuponesInvalidos = $session['cuponesInvalidos'];

        $invalidos = "";

        for ($i=0; $i < count($cuponesInvalidos) ; $i++) { 
            $invalidos .= $cuponesInvalidos[$i].", ";
        }

        $this->session->sess_destroy();

        if($guardados == 0){

            $response = array(
                "code"    =>    305,
                "mensaje" =>    "Uno o más de los códigos que ingresaste no son válidos. Por favor verifícalo, . Invalidos (". $invalidos.")"
            );
            echo json_encode($response);
            return;

        }else if($noGuardados == 0){
            $response = array(
                "code"    =>    1,
                "mensaje" =>    "Tus cupones se han registrado con éxito"
            );
            echo json_encode($response);
            return;
        }else {
        
            $response = array(
                "code"    =>    1,
                "mensaje" =>    "Se guardaron " . $guardados . " cupones validos de los que ingresastes. Invalidos (". $invalidos.")"
            );
            echo json_encode($response);
            return;
        }

    }
    
    public function registrarUsuarios(){
        $data = $this->input->post(null,true);

        if(!isset($data['tipoDocumento']) || empty($data['tipoDocumento'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que selecionar el tipo de documento"
            );
            echo json_encode($response);
            return;
		}
		
		if(!isset($data['numeroDocumento']) || empty($data['numeroDocumento'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que ingresar el numero de documento"
            );
            echo json_encode($response);
            return;
        }
        
        if(!isset($data['nombreCompleto']) || empty($data['nombreCompleto'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tiene que ingresar el tu nombre completo no pùede estar vacio el campo."
            );
            echo json_encode($response);
            return;
        }

        if(!isset($data["email"]) || empty($data["email"]) || !filter_var($data["email"], FILTER_VALIDATE_EMAIL) ){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "El correo no es valido"
            );
            echo json_encode($response);
            return;
        }
            
        if(!isset($data["pass"]) || empty($data["pass"])){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "password inválido, o  no puede estar vacio"
            );
            echo json_encode($response);
            return;
        }

        if(!isset($data['tlfContacto']) || empty($data['tlfContacto'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tienes que ingresar el teléfono de contacto"
            );
            echo json_encode($response);
            return;
        }
        
        if(!isset($data['cuidad']) || empty($data['cuidad'])){
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Por favor tienes que ingresar LA CIUDAD"
            );
            echo json_encode($response);
            return;
        }

        //creo el hash para verificar el email de los usuarios
        //$hash = $this->crearHash();

        $credenciales = array(
            'email' => $data['email'],
            'pass' => md5($data['pass']),
            'rol' => 1,
            'estado' => 1
        );

        //validos si existe el email en base de datos

        $existeEmail = $this->Usuario_model->validarEmail($data['email']);

        if (isset($existeEmail) && !empty($existeEmail)) {
            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Este correo ya ha sido registrado antes. Solo puedes crear una cuenta por correo."
            );
            echo json_encode($response);
            return;
        }

        $idUsuario = $this->Usuario_model->insertUsuario($credenciales, $data);

        if(isset($idUsuario) && $idUsuario > 0){

            $guardados = $this->validarCupones($idUsuario);

            $session = $this->session->all_userdata();

            $noGuardados = count($session['cupones']) - $guardados;

            //cupones Invalidos
                $cuponesInvalidos = $session['cuponesInvalidos'];

                $invalidos = "";

                for ($i=0; $i < count($cuponesInvalidos) ; $i++) { 
                    $invalidos .= $cuponesInvalidos[$i].", ";
                }
            //cupones Invalidos End

            $this->session->sess_destroy();

            if($guardados == 0){

                $response = array(
                    "code"    =>    1,
                    "mensaje" =>    "Tu cuenta ha sido creada pero los códigos que ingresaste no son válidos. Por favor verifícalos, Invalidos (". $invalidos.")"
                );
                echo json_encode($response);
                return;
    
            }
            if($noGuardados == 0){

                $response = array(
                    "code"    =>    1,
                    "mensaje" =>    "Tu cuenta ha sido creada y tus cupones se han guardado con éxito"
                );
                echo json_encode($response);
                return;

            }else {
                $response = array(
                    "code"    =>    1,
                    "mensaje" =>    "Tu cuenta ha sido creada Cupones registrados: " . $guardados . " de " . count($session['cupones']) . ". Verifica los demás códigos, Invalidos (". $invalidos.")"
                );
                echo json_encode($response);
                return;
            }

        }else {

            $response = array(
                "code"    =>    0,
                "mensaje" =>    "Error al registrar al usuario"
            );
            echo json_encode($response);
            return;

        }
    }

    public function prueba(){
        $restulSend =$this->send->enviarEmailCliente('ricardojavier8058@gmail.com',"asdasd");

        if( $restulSend) {
            echo "enviado";
        }
    }


    public function validarCupones($idUsuario){

        $cuponesInvalidos = array();

        $session = $this->session->all_userdata();
        $cupones = $session['cupones'];

        $insertados = 0;

        for ($i=0; $i < count($cupones); $i++) { 
            
            $existeCupon = $this->Usuario_model->validarCupon($cupones[$i]);

            if(isset($existeCupon) && count($existeCupon) > 0){

                //valido si el cupones esta activo, para poder usarlo tiene que estar en 1
                if($existeCupon[0]['estatus'] == 1){

                    $fechaRegistro = date('Y-m-d');

                    $array = array(
                        'id_users' => $idUsuario,
                        'cupon' => $existeCupon[0]['cupon'],
                        'fechaRegistro' => $fechaRegistro,
                        'mes' => date('n')
                    );

                    $idCuponUsuario = $this->Usuario_model->insertCuponesUsuarios($array);

                    if($idCuponUsuario > 0){
                        //cambio de estatus el cupon ingresado 
                        $updateCupon = array(
                            'estatus' => 0
                        );

                        $this->Usuario_model->updateCupon($existeCupon[0]['id_cupones'],$updateCupon);
                        
                        $insertados++;
                    }else {
                        $cuponesInvalidos[] = $cupones[$i];
                    }

                }else {
                    $cuponesInvalidos[] = $cupones[$i];
                }
            }else {
                $cuponesInvalidos[] = $cupones[$i];
            }
        }

        $this->session->set_userdata("cuponesInvalidos", $cuponesInvalidos);

        return $insertados;
    }

    private function crearHash() {

        $length = 32;

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    } 


}
