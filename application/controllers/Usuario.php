<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Usuario_model");

		$session = $this->session->all_userdata();

        //print_r($session);
        if($session["isLoggedIn"] != true && $session["id_usuario"] == 0){ 
            $base_url = base_url();
            redirect($base_url);
        }

	}
	
	public function index()
	{
		$user = $this->Usuario_model->dataInfoUser($this->session->id_usuario);

		$datos = array(
			'user' => $user
		);

		$this->load->view('perfil_view',$datos);
	}

	public function updatePerfil(){

        $data = $this->input->post(null,true);
        
        if(isset($data['id_users']) && !empty($data['id_users'])){
            $id_user = $data['id_users'];
        }else {
            $id_user = $this->session->id_usuario;
        }

		if(!isset($data["email"]) || empty($data["email"]) || !filter_var($data["email"], FILTER_VALIDATE_EMAIL) ){
            $response = array(
                "code"    =>    100,
                "mensaje" =>    "usuario invalido"
            );
            echo json_encode($response);
            return;
        }

        //array credenciales
        $update = array(
        	'email' => $data['email']
        );

        //array info
        $updateInfo = array(
        	'tipoDocumento' => $data['tipoDocumento'],
            'numeroDocumento' => $data['numeroDocumento'],
        	'nombreCompleto' => $data['nombreCompleto'],
        	'tlfContacto' => $data['tlfContacto'],
        	'cuidad' => $data['cuidad'],
        );
        
        //valido si se desea cambiar la contraseña
        if(isset($data['mostarPass']) && $data['mostarPass'] == 1){
            //si existe el pass se la agrego al aarray de update de credeciales
            if (isset($data['pass']) && !empty($data['pass'])) {
                $update['pass'] = md5($data['pass']);
            }else {
                $response = array(
                    "code"    => 0,
                    "mensaje" => "Si deseas cambiar la contraseña, no pueden estar vacios los campos."
                );
                echo json_encode($response);
                return;
            }

        }

        

        $this->Usuario_model->actualizarUser($id_user ,$update,$updateInfo);
		
		$response = array(
            "code"    => 1,
            "mensaje" => "Tus datos han sido actualizados"
        );
        echo json_encode($response);
        return;

	}


	private function cargarArchivos($config,$nombre){

        $this->load->library("upload", $config);

        if($this->upload->do_upload($nombre)){
            
            $dataFile = $this->upload->data();
            
            return $dataFile['file_name'];

        }else{
            $respuesta = array(
                'code' => 0,
                'mensaje' => $this->upload->display_errors()
            );

            echo json_encode($respuesta);
            //echo $this->upload->display_errors();
            return;
        }
    }

    public function editar($id_users = null){
        if($id_users != null && $this->session->rol == 0) {

            $data = array(
                'user' => $this->Usuario_model->dataInfoUser($id_users)
            );
            $this->load->view('editarUsuario_view',$data);
        } else {
            $this->load->view('error_view');
        }
    }
    
}

/*End of dile Usuario.php*/
