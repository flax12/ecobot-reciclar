<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model("Usuario_model");

		$session = $this->session->all_userdata();

        //print_r($session);
        if($session["isLoggedIn"] != true && $session["id_usuario"] == 0){ 
            $base_url = base_url();
            redirect($base_url);
        }
	}
	
	public function index()
	{
		
		if($this->session->rol == 1) {
			/*$botellas = array(
				"Enero" => $this->contarBotellas($this->session->id_usuario,1),
				"febrero" => $this->contarBotellas($this->session->id_usuario,2),
				"Marzo" => $this->contarBotellas($this->session->id_usuario,3),
				"Abril" => $this->contarBotellas($this->session->id_usuario,4),
				"Mayo" => $this->contarBotellas($this->session->id_usuario,5),
				"Junio" => $this->contarBotellas($this->session->id_usuario,6),
				"Julio" => $this->contarBotellas($this->session->id_usuario,7),
				"Agosto" => $this->contarBotellas($this->session->id_usuario,8),
				"Septiembre" => $this->contarBotellas($this->session->id_usuario,9),
				"octubre" => $this->contarBotellas($this->session->id_usuario,10),
				"Noviembre" => $this->contarBotellas($this->session->id_usuario,11),
				"Diciembre" => $this->contarBotellas($this->session->id_usuario,12),
	
			);*/

			//mes actual
			$mesActual = date('n');
			$mesPasado = $mesActual - 1;

			if($mesActual == 1) {
				$botellas['Enero'] = $this->contarBotellas($this->session->id_usuario,1);
			}else if($mesActual == 2){
				$botellas['febrero'] = $this->contarBotellas($this->session->id_usuario,2);
			}else if($mesActual == 3){
				$botellas['Marzo'] = $this->contarBotellas($this->session->id_usuario,3);
			}else if($mesActual == 4){
				$botellas['Abril'] = $this->contarBotellas($this->session->id_usuario,4);
			}else if($mesActual == 5){
				$botellas['Mayo'] = $this->contarBotellas($this->session->id_usuario,5);
			}else if($mesActual == 6){
				$botellas['Junio'] = $this->contarBotellas($this->session->id_usuario,6);
			}else if($mesActual == 7){
				$botellas['Julio'] = $this->contarBotellas($this->session->id_usuario,7);
			}else if($mesActual == 8){
				$botellas['Agosto'] = $this->contarBotellas($this->session->id_usuario,8);
			}else if($mesActual == 9){
				$botellas['Septiembre'] = $this->contarBotellas($this->session->id_usuario,9);
			}else if($mesActual == 10){
				$botellas['Octubre'] = $this->contarBotellas($this->session->id_usuario,10);
			}else if($mesActual == 11){
				$botellas['Noviembre'] = $this->contarBotellas($this->session->id_usuario,11);
			}else if($mesActual == 12){
				$botellas['Diciembre'] = $this->contarBotellas($this->session->id_usuario,12);
			}

			if($mesPasado == 1) {
				$botellas['Enero'] = $this->contarBotellas($this->session->id_usuario,1);
			}else if($mesPasado == 2){
				$botellas['febrero'] = $this->contarBotellas($this->session->id_usuario,2);
			}else if($mesPasado == 3){
				$botellas['Marzo'] = $this->contarBotellas($this->session->id_usuario,3);
			}else if($mesPasado == 4){
				$botellas['Abril'] = $this->contarBotellas($this->session->id_usuario,4);
			}else if($mesPasado == 5){
				$botellas['Mayo'] = $this->contarBotellas($this->session->id_usuario,5);
			}else if($mesPasado== 6){
				$botellas['Junio'] = $this->contarBotellas($this->session->id_usuario,6);
			}else if($mesPasado == 7){
				$botellas['Julio'] = $this->contarBotellas($this->session->id_usuario,7);
			}else if($mesPasado == 8){
				$botellas['Agosto'] = $this->contarBotellas($this->session->id_usuario,8);
			}else if($mesPasado == 9){
				$botellas['Septiembre'] = $this->contarBotellas($this->session->id_usuario,9);
			}else if($mesPasado == 10){
				$botellas['Octubre'] = $this->contarBotellas($this->session->id_usuario,10);
			}else if($mesPasado == 11){
				$botellas['Noviembre'] = $this->contarBotellas($this->session->id_usuario,11);
			}else if($mesPasado == 12){
				$botellas['Diciembre'] = $this->contarBotellas($this->session->id_usuario,12);
			}

			$data['botellas'] = $botellas;
			$data['total'] = count($this->Usuario_model->usuariosCuponesTodos($this->session->id_usuario));
	
			$this->load->view('home_panel_view.php',$data);

		} else {
			$registros = $this->Usuario_model->getRegistros();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('usuariosRegistrados_view',$data);
		}
	}

	public function contarBotellas($idUser,$mes){
		$data = $this->Usuario_model->usuariosMesCupones($idUser,$mes);
		return count($data);
	}

	//metodo para la vista de registrar cupones
	public function vistaRegistroCupones(){

		if ($this->session->rol == 0) {

			$this->load->view('registroCupones_view');
		}else{
			$this->load->view('error_view',$data);

		}
	}

	public function insertCupon() {
		$data = $this->input->post(null, true);

		if(!isset($data['cupon']) || empty($data['cupon'])){
			$response = array(
				'code' => 0,
				"mensaje" => "Por favor ingrese un cupon"
			);
			echo json_encode($response);
			return;
		}

		//valido si el cupon existe

		$existeCupon = $this->Usuario_model->validarCupon($data['cupon']);
		
		if(isset($existeCupon) && count($existeCupon) > 0){
			$response = array(
				'code' => 0,
				"mensaje" => "El cupon: ". $data['cupon'] . " Ya esta registrado, intente con otro por favor."
			);
			echo json_encode($response);
			return;
		}

		$data['estatus'] = 1;
		$idCupon = $this->Usuario_model->insertCupon($data);

		if (!$idCupon || empty($idCupon) || $idCupon == 0) {
			$response = array(
				'code' => 0,
				"mensaje" => "No se pudo realizar el registro"
			);
			echo json_encode($response);
			return;
		}

		$response = array(
			'code' => 1,
			"mensaje" => "Cupon Registrado con exito"
		);
		echo json_encode($response);
		return;
	}

	public function insertCsvCupones(){
		$data = $this->input->post(null,true);

		$formatos_permitidos =  array('csv');
		$archivo = $_FILES['archivo']['name'];
		$extension = pathinfo($archivo, PATHINFO_EXTENSION);
		if(!in_array($extension, $formatos_permitidos) ) {
			$response = array(
				'code' => 0,
				"mensaje" => "El archivo tiene que tener extension .csv, intenta con otro por favor"
			);
			echo json_encode($response);
			return;
		}

		//si existe le agrego al update el nombre del archivo
		if(isset($_FILES['archivo']['name']) && $_FILES['archivo']['name']!=""){

			//subo el archivo a la carpta creada anteriormente
	        $config = array(
	            "upload_path" => "./archivos",
	            'allowed_types' => "csv"
	        );

	        $nombreArchivo = $this->cargarArchivos($config,'archivo');
		}

		$pathArchivo = "./archivos/".$nombreArchivo;
		
		//ya guardadi y cargado al servidor prosigo a abrirlo y cargar la data en base de datos
		if (file_exists($pathArchivo)) {
			
			$row = 1;

			if (!$fichero = fopen($pathArchivo, "r")){
				
				$response = array(
					'code' => 0,
					"mensaje" => "No se pudo abrir el archivo: " . $nombreArchivo
				);
				echo json_encode($response);
				return;

			}else{
				// Lee los registros
				while (($datos = fgetcsv($fichero, 0, ",", "\"", "\"")) !== FALSE) {
			
					if ($row != 1) {
			
						$insertCupon = array(
							'cupon' => $datos[0],
							'estatus' => $datos[1]
						);

						$existeCupon = $this->Usuario_model->validarCupon($datos[0]);

						if( !isset($existeCupon) || empty($existeCupon) ){
							$idCupon = $this->Usuario_model->insertCupon($insertCupon);
						}

					}
			
					$row++;      
				}

				$response = array(
					'code' => 1,
					"mensaje" => "los cupones del archivo: " . $nombreArchivo . " se han registrado con exito"
				);
				echo json_encode($response);
				return;
			} 

		} else {
			
			$response = array(
				'code' => 0,
				"mensaje" => "Se intento acceder a un archivo que no existe en el servidor. intenta nuevamente por favor."
			);
			echo json_encode($response);
			return;
		}

	}

	public function tableCupones(){

		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getCupones();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('tablaCupones_view',$data);
		}else{
			$this->load->view('error_view',$data);

		}
		
	}

	public function deleteCupon(){
		$data = $this->input->post(null, true);

		try {

			$this->Usuario_model->eleminarCupon($data['idCupon']);
			$response = array(
				'code' => 1,
				"mensaje" => "Cupon Eliminado con exito"
			);
			echo json_encode($response);
			return;

		} catch (\Throwable $th) {

			$response = array(
				'code' => 0,
				"mensaje" => "No se pudo eliminar El registro"
			);
			echo json_encode($response);
			return;
		}
	}

	public function editarCupon(){
		$data = $this->input->post(null,true);

		$update = array(
			'cupon' => $data['cupon']
		);

		if(!isset($data['cupon']) || empty($data['cupon'])){
			$response = array(
				'code' => 0,
				"mensaje" => "Por favor ingrese un cupon"
			);
			echo json_encode($response);
			return;
		}

		try {
			$this->Usuario_model->updateCupon($data['idCuponEditar'],$update);

			$response = array(
				'code' => 1,
				"mensaje" => "Cupon Actualizado con exito"
			);
			echo json_encode($response);
			return;

		} catch (\Throwable $th) {

			$response = array(
				'code' => 0,
				"mensaje" => "No se pudo Actualizar el cupon"
			);
			echo json_encode($response);
			return;

		}
	}

	public function cuponesUsuarios(){
		
		if ($this->session->rol == 1) {
			$registros = $this->Usuario_model->getCuponesUser($this->session->id_usuario);

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('usuarioCupones_view',$data);
		}else{
			$this->load->view('error_view',$data);

		}

	}

	public function usuariosRegistrados(){
		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getRegistros();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('usuariosRegistrados_view',$data);
		}else{
			$this->load->view('error_view',$data);

		}
	}

	public function deleteUser(){
		$data = $this->input->post(null,true);

		try {

			$this->Usuario_model->deleteUsuario($data);
			$response = array(
				'code' => 1,
				"mensaje" => "Usuario Eliminado con exito"
			);
			echo json_encode($response);
			return;

		} catch (\Throwable $th) {

			$response = array(
				'code' => 0,
				"mensaje" => "No se pudo eliminar El Usuario"
			);
			echo json_encode($response);
			return;
		}
	}

	public function cuponesRegistradosPorTodosUsuarios(){
		
		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getCuponesTodoUser();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('tablaCuponesTodosUsuarios_view',$data);
		}else{
			$this->load->view('error_view',$data);

		}
	}

	public function excelCuponesRegistradosUsuarios(){

		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getCuponesTodoUser();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('excel_view/resporteCuponesRegistrados',$data);
		}else{
			$this->load->view('error_view',$data);

		}
	}

	public function excelCupones(){
		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getCupones();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('excel_view/reporteCupones',$data);
		}else{
			$this->load->view('error_view',$data);

		}
	}

	public function excelUsuarios(){
		if ($this->session->rol == 0) {
			$registros = $this->Usuario_model->getRegistros();

			$data = array(
				'data' => $registros
			);
			
			$this->load->view('excel_view/reporteUsuarios',$data);
		}else{
			$this->load->view('error_view',$data);

		}
	}

	private function cargarArchivos($config,$nombre){

        $this->load->library("upload", $config);

        if($this->upload->do_upload($nombre)){
            
            $dataFile = $this->upload->data();
            
            return $dataFile['file_name'];

        }else{
            $respuesta = array(
                'code' => 0,
                'mensaje' => $this->upload->display_errors()
            );

            echo json_encode($respuesta);
            //echo $this->upload->display_errors();
            return;
        }
	}
	
	public function DeleteCuponesSeleccionados() {
		$data = $this->input->post(null,true);



		if (!isset($data['cupones']) || count($data['cupones']) == 0) {
			$respuesta = array(
				'code' => 0,
				'mensaje' => "Tienes que selecionar por lo menos un código"
			);

			echo json_encode($respuesta);
            return;
		}

		try {

			for ($i=0; $i < count($data['cupones']) ; $i++) { 
				$this->Usuario_model->eleminarCupon($data['cupones'][$i]);
			}

			$response = array(
				'code' => 1,
				"mensaje" => "códigos Eliminados con exito"
			);
			echo json_encode($response);
			return;

		} catch (\Throwable $th) {

			$response = array(
				'code' => 0,
				"mensaje" => "No se pudo eliminar los registros"
			);
			echo json_encode($response);
			return;
		}

		
	}

}

/*End of file Panel.php*/
